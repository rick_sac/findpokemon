<?php
  require_once "../includes/header.php";

  //get the select pokemon name
  $pokemonName = $_GET["selectPokemonName"];

  //search for the pokemon name
  $api = curl_init("https://pokeapi.co/api/v2/pokemon/$pokemonName");
  curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
  //get the responde
  $response = curl_exec($api);
  curl_close($api);

  //decode the json string
  $json = json_decode($response);
  ?>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <table style="width:100%">
    <tr>
      <th>Name</th>
      <th>Species</th>
      <th>Height</th>
      <th>Weight</th>
      <th>Abilities</th>
    </tr>
    <tr>
      <td><?=$json->name?></td>
      <td><?=$json->species->name?></td>
      <td><?=$json->height?></td>
      <td><?=$json->weight?></td>
      <td>
      <?php
      foreach($json->abilities as $k => $v) {
        echo $v->ability->name.'<br>';
      }
      ?>
      </td>
    </tr>
  </table>
  <div class="pokemon">
    <?php
      echo '<img src="'.$json->sprites->back_default.'" width="300">';
      echo '<img src="'.$json->sprites->front_default.'" width="300">';
    ?>
  </div><br>
  <div class="buttonBack">
    <a class="back" title="Back to Main Menu" href="../public/index.php">BACK</a>
  </div>
</body>
</html>

