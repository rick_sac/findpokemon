<?php
  require_once "../includes/header.php";
  //some pokemon names
  $pokemon = array("bulbasaur","ivysaur","venusaur","charmander","charmeleon","charizard","squirtle","wartortle","blastoise","caterpie","metapod","butterfree","weedle","kakuna","beedrill","pidgey","pidgeotto","pidgeot","rattata","raticate","spearow","fearow","ekans","arbok","pikachu","raichu","sandshrew","sandslash","nidoran","nidorina","nidoqueen","nidoran","nidorino","nidoking","clefairy","clefable","vulpix","ninetales","jigglypuff","wigglytuff","zubat","golbat","oddish","gloom","vileplume","paras","parasect","venonat","venomoth","diglett","dugtrio","meowth","persian","psyduck","golduck","mankey","primeape","growlithe","arcanine");
?>

  <div class="initial">
    <h1>Check Pokemon Super Abilities</h1>
    <form name="selectedPokemonName" action="../public/processing.php">
      <h4>Please select your favorite pokemon name</h4>
      <select name="selectPokemonName" required >
        <option disabled selected value> -- Select name option -- </option>
          <?php 
            foreach($pokemon as $pokemonName){
              echo "<option>".$pokemonName."</option>";
            }
          ?>
      </select><br><br><br>
      <input class="submit" type="submit" value="Submit">
    </form>
    <br> 
    <footer>
      <i><small>&copy; Ricardo Lopes <?php echo date('Y');?></small></i>   
    </footer>
  </div>
  </body>
</html>

